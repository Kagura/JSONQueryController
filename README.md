# JSONQueryController
在SpringBoot Controller中使用自定义注解用JSONQuery简化解析Post提交的JSON

#### 用到的技术:
>* SpringBoot Web/Aop
>* Annotation
>* [JSONQuery](https://github.com/KingFalse/JSONQuery)
>* MockMvc

#### 示例代码 [Controller.java](https://github.com/KingFalse/JSONQueryController/blob/master/src/main/java/me/kagura/controller/Controller.java)
~~~java
    //@PostMapping(value = "/post")
    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String hello(
            @JSONBodyField Integer errno,
            @JSONBodyField("user > user_name") String username,
            @JSONBodyField("comment_info > [-1]") CommentInfo lastCommentInfo
    ) {
        System.err.println("errno = " + errno);
        System.err.println("username = " + username);
        System.err.println("lastCommentInfo = " + lastCommentInfo);
        return "{\"code\":\"200\"}";
    }
~~~

#### 可以从本项目学习到的点：
> * 使用SpringBoot Web模块创建一个Restful风格的接口服务
> * 自定义注解的开发
> * 使用Aop处理自定义注解
> * 使用MockMvc编写单元测试

#### 版本:
>* SpringBoot 2.0.0
>* [JSONQuery 0.2.5](https://github.com/KingFalse/JSONQuery)
