package me.kagura.annotation;

import java.lang.annotation.*;

/**
 * Created by ZFang on 2017/11/22.
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JSONBodyField {
    String value() default "";
}
