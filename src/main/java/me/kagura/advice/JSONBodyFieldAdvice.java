package me.kagura.advice;

import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLDecoder;

/**
 * Created by ZFang on 2017/11/22.
 */
@ControllerAdvice
public class JSONBodyFieldAdvice {

    @ModelAttribute
    public void saveBodyString(HttpEntity<String> httpEntity, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            String data = URLDecoder.decode(httpEntity.getBody(),"UTF-8");
            request.setAttribute("jsonBodyString", data);
        }

    }

}