package me.kagura.entity;

public class CommentInfo {

    private String tid;
    private String pid;
    private String cid;

    @Override
    public String toString() {
        return "CommentInfo{" +
                "tid='" + tid + '\'' +
                ", pid='" + pid + '\'' +
                ", cid='" + cid + '\'' +
                '}';
    }

}
