package me.kagura;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonQueryControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonQueryControllerApplication.class, args);
	}
}
