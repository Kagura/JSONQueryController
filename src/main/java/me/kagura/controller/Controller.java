package me.kagura.controller;

import me.kagura.annotation.JSONBodyField;
import me.kagura.entity.CommentInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    //@PostMapping(value = "/post")
    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String hello(
            @JSONBodyField Integer errno,
            @JSONBodyField("user > user_name") String username,
            @JSONBodyField("comment_info > [-1]") CommentInfo lastCommentInfo
    ) {
        System.err.println("errno = " + errno);
        System.err.println("username = " + username);
        System.err.println("lastCommentInfo = " + lastCommentInfo);
        return "{\"code\":\"200\"}";
    }

}
