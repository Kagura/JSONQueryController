package me.kagura.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class ControllerTest {
    @Autowired
    private MockMvc mock;

    private String json = "" +
            "{\n" +
            "  \"errno\": 0,\n" +
            "  \"errmsg\": 成功,\n" +
            "  \"user\": \"{\\\"user_id\\\":643361255,\\\"user_name\\\":\\\"鹞之神乐\\\",\\\"user_sex\\\":1,\\\"user_status\\\":1}\",\n" +
            "  \"comment_info\": [\n" +
            "    {\n" +
            "      \"tid\": \"5504460056\",\n" +
            "      \"pid\": \"116776960983\",\n" +
            "      \"cid\": \"116857893053\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"tid\": \"5504460056\",\n" +
            "      \"pid\": \"116776960983\",\n" +
            "      \"cid\": \"116858057626\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"tid\": \"5504460056\",\n" +
            "      \"pid\": \"116776960983\",\n" +
            "      \"cid\": \"116880757453\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"data\": {\n" +
            "    \"comment_list\": {\n" +
            "      \"116776891765\": {\n" +
            "        \"comment_num\": 3,\n" +
            "        \"comment_list_num\": 4\n" +
            "      },\n" +
            "      \"116776960983\": {\n" +
            "        \"comment_num\": 4,\n" +
            "        \"comment_list_num\": 4\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    @Test
    public void hello() throws Exception {
        mock.perform(
                MockMvcRequestBuilders.post("/post")
                        .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                        .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk()
                );
    }

}